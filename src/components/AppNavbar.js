import {useContext} from 'react'
import {Link, NavLink, Outlet} from 'react-router-dom'
import {NavDropdown,Nav,Navbar,Container} from 'react-bootstrap'
import UserContext from '../UserContext.js'


export default function AppNavbar(){
	const {user, setUser} = useContext(UserContext);

	console.log(user.isAdmin)
	return(
	<Container fluid className="px-0">
		<Navbar expand="lg" className="navBar" >
		<Navbar.Brand as={NavLink} exact to="/" className="nav-title navbar-dark">7 Evelyn
		</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id ="basic-navbar-nav">
		<div className="navm-right px-2 navbar-nav ml-auto" >
			<Nav className="navMenu">
					
				{	

			(user.token !== null && user.isAdmin !== false)?
			<>	
			
				<Nav.Link  className="adminNavLinks" as={NavLink} exact to="/">Home</Nav.Link>

				<Container fluid>

			<NavDropdown title="Admin" id="basic-nav-dropdown" className="navLinkss">
					<NavDropdown.Item as={NavLink} exact to="/admin">Admin Board</NavDropdown.Item>
	             
	              <NavDropdown.Item as={NavLink} exact to="/logout">Logout</NavDropdown.Item>
			</NavDropdown>

				</Container>
{/*
				<Nav.Link className="nav" as={NavLink} exact to="/admin">Admin board</Nav.Link>
				<Nav.Link className="nav float-right" as={NavLink} exact to="/logout">Logout</Nav.Link>*/}
			</>
		:(user.token !== null && user.isAdmin !==true)?
			<>

			<Nav.Link  className="nav" as={NavLink} exact to="/">Home</Nav.Link>
			
			<Nav.Link  className="nav" as={NavLink} exact to="/products">Products</Nav.Link>
			{/*
			<Nav.Link >Cart</Nav.Link>
			*/}
		<Container fluid>

			<NavDropdown title="User" id="basic-nav-dropdown" className="navLinkss">
					{/*<NavDropdown.Item as={NavLink} exact to="/admin">Admin Board</NavDropdown.Item>*/}
	              <NavDropdown.Item as={NavLink} exact to="/profile">Profile</NavDropdown.Item>
	              <NavDropdown.Item as={NavLink} exact to="/logout">Logout</NavDropdown.Item>
			</NavDropdown>
				</Container>
				</>
		:
		<>		
			
			<Nav.Link  className="navLinks" as={NavLink} exact to="/">Home</Nav.Link>
			
			<Nav.Link  className="navLinks" as={NavLink} exact to="/products">Products</Nav.Link>
			
			<Nav.Link className="navLinks" as={NavLink} exact to="/login">Login</Nav.Link>
	
			<Nav.Link className="navLinks" as={NavLink} exact to="/register">Register</Nav.Link>
		</>
					}
				</Nav>
			</div>
			</Navbar.Collapse>
		
		</Navbar>
	</Container>


		)
}