import {Row,Col, Card,Button,CardGroup, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Highlights({productProps}){

     const {productName,productBrand,description, price, _id, isActive} = productProps;
     let title1=productName;
     
	return(

   <div className="card-container">
      <Card className="cardHighlight m-3">
        <Card.Body className="d-flex flex-column card-body">
         
          <div>
            <Card.Title className="pb-2">{productName}</Card.Title>
            <Card.Subtitle>Php {price}</Card.Subtitle>
            <br/>
            <Card.Subtitle className="pb-2">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
          </div>
          
            
            
              
            <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
            
          
        </Card.Body>
      </Card>
    </div>



		)
}