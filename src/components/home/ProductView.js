import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col,Image,InputGroup, FormControl, ListGroup,Form } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal';
import UserContext from '../../UserContext.js'
import Swal from 'sweetalert2'
import {BsFillBagPlusFill, BsFillBagDashFill} from 'react-icons/bs'
export default function ProductView() {

	const [showOrder, setShowOrder] = useState(false);
    const handleCloseOrder = () => setShowOrder(false);
					
	const navigate = useNavigate();
	const { user } =useContext(UserContext);
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [brand, setBrand] = useState("");
	const [price, setPrice] =useState(0);
	const [count , setCount] = useState(1)
	const total= price * count
	useEffect(() =>{
		
		fetch(`${process.env.REACT_APP_API_URL }/products/${productId}`)
				.then(res => res.json())
				.then(data => {

			

					setName(data.productName);
					setDescription(data.description);
					setBrand(data.productBrand);
					setPrice(data.price);

				});
	}, [])
	
 function orderProduct(event) {
            event.preventDefault();
      
            fetch(`${process.env.REACT_APP_API_URL }/users/myOrder`,{
                method: 'POST',
                headers: {
                		Authorization: `access ${user.token}`,
                        'Content-Type': 'application/json; charset=UTF-8'
                 },
                body: JSON.stringify({
                    productId: `${productId}`,
                    productName: name,
                    productBrand: brand,
                    price: price,
                    quantity: count
                },{
                	totalAmount: total
                })
            },[])
                Swal.fire({

                            title: "Ordered Successfully",
                            icon: "success"
                })
                          

    }
	
	return(
		<>
		<Container className="mt-5 viewProduct">
		<div className="bg-itempage">
			<div >

				<div >
					<div >
						<h1 className="viewName">{name}</h1>
						<p>{description}</p>
						<h4 className="viewPrice">Php {price}</h4>
					</div>
					{
						(user.id !== null)?
					<>	
					<div>
								<BsFillBagPlusFill onClick={() => setCount(count + 1)} className="plus-count" size={40}/>			
								{count}
								<BsFillBagDashFill onClick={() => count > 1 && 	setCount(count - 1)} className="plus-count" size={40}/>	
							</div>
					<Button  variant="primary" className="m-2 viewButton" onClick={() => setShowOrder(true)}>
						Buy Now
					</Button>
					</>
						:
						<Link class="btn btn-primary btn-block" to="/login">Login</Link>

					}	

					<Link className="btn btn-primary btn-block m-3" to="/">Back</Link>
				</div>
			</div>
		</div>
		</Container>

		 <Modal
                show={showOrder}
                onHide={() => setShowOrder(false)}
                dialogClassName="modal-90w"
                aria-labelledby="lol"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="">
                    Order
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form  className="mt-4" onSubmit={event=> {orderProduct(event) } }>
                         <Form.Group className="mb-3" controlId="productName">
                            <Form.Label hidden>`${productId}`</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-1" controlId="productName">
                            <Form.Label>Name: {name}</Form.Label>
                        </Form.Group>
                       
                        <Form.Group className="mb-1" controlId="description">
                            <Form.Label>Desc: {description}</Form.Label>   
                        </Form.Group>
                        <Form.Group className="mb-1" controlId="price">
                            <Form.Label>Quantity: {count}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-1" controlId="price">
                            <Form.Label>Total Price: ₱{total}</Form.Label>
                        </Form.Group>  
                        <Button variant="primary" type="submit" controlId="submitBtn" onClick={handleCloseOrder}>Order</Button>
                    </Form>
                </Modal.Body>
              </Modal>
             </>
		)
}