
import {Button, Row, Col, Container,Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'
export default function Banner() {
	return(
		<>
	<Container className="bannerDesign">
		<Row >
		<Col className="p-5 text-center">
			
				<h1 className="homeTitle">7 / Evelyn</h1>
				<h3 className="homeSub">Where you can buy everything!</h3>

				<Button variant="primary" ><Nav.Link  as={NavLink} exact to="/products">Get Started :)</Nav.Link></Button>
			</Col>
		</Row>
	</Container>	
	</>
		)
}