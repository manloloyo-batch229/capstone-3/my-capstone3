import {Container,Row,Col, InputGroup, FormControl, ListGroup, Button, Card, Table ,Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal';
import UserContext from '../../UserContext.js'
import Swal from 'sweetalert2'
import 'bootstrap/dist/css/bootstrap.min.css'
import Banner from '../home/Banner.js'
export default function AllProducts({productProps}) {
    const { user } =useContext(UserContext);
    const [isOpen, setIsOpen] = useState(false);

      const openModal = () => {
        setIsOpen(true);
      };

      const closeModal = () => {
        setIsOpen(false);
      };
    const [showUpdate, setShowUpdate] = useState(false);
    const handleCloseUpdate = () => setShowUpdate(false);

    const [showDelete, setShowArchive] = useState(false);
    const handleCloseDelete = () => setShowArchive(false);

    const {productName,productBrand,description, price, _id, isActive} = productProps;

    const [searchInput, setSearchInput] = useState('');
    
    const [prodName, setProductName] = useState(productName);
    const [prodBrand,setProductBrand] = useState(productBrand);
    const [prodDes,setDescription] = useState(description);
    const [prodPrice,setPrice] = useState(price);

    useEffect(() => {  
        console.log(searchInput)

    },[searchInput])

    function updateProduct(event) {
            event.preventDefault();
      
            fetch(`${process.env.REACT_APP_API_URL }/products/${_id}`,{
                method: 'PUT',
                headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                            },
                body: JSON.stringify({
                    productName: prodName,
                    productBrand: prodBrand,
                    description: prodDes,
                    price: prodPrice
                })
            },[])
                Swal.fire({

                            title: "Product Added Successfully",
                            icon: "success",
                }).then((result) => {
                      if (result.value) {
                        window.location.reload();
                      }
                  })
                          

    }

    function archiveProduct(event) {
            event.preventDefault();
      
            fetch(`${process.env.REACT_APP_API_URL }/products/${_id}/archive`,{
                method: 'PUT',
                headers: {
                        Authorization: `access ${user.token}`,
                        'Content-Type': 'application/json; charset=UTF-8'
                            },
                body: JSON.stringify({
                    isActive: false,
                    
                })
            },[])
                Swal.fire({

                            title: "Product Archived Successfully",
                            icon: "success",
                }).then((result) => {
                      if (result.value) {
                        window.location.reload();
                      }
                  })
                          

    }
    console.log(productName);
	return (  
        <>
			<Card className="m-4">
                <Card.Body>
                    <Card.Title hidden>{_id}</Card.Title>
                    <Card.Title>{productName}</Card.Title>
                    <Card.Subtitle>{productBrand}</Card.Subtitle>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button variant="primary" onClick={() => setShowUpdate(true)} className="mx-2">
                        Update
                    </Button>
                    <Button variant="danger" onClick={() => setShowArchive(true)}>
                        Archive
                    </Button>
                    
                    
                </Card.Body>
             </Card>

 {/*Modal for Update*/}            
            <Modal
                show={showUpdate}
                onHide={() => setShowUpdate(false)}
                dialogClassName="modal-90w"
                aria-labelledby="lol"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="lol">
                    Update Product
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={event=> {updateProduct(event) } } className="mt-4">
                         <Form.Group className="mb-3" controlId="productName">
                            <Form.Label >{_id}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control type="text" placeholder="Product name"  value={prodName} onChange={event => setProductName(event.target.value)} required/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="productBrand">
                            <Form.Label>Product Brand</Form.Label>
                            <Form.Control type="text" placeholder="Brand name"  value={prodBrand} onChange={event => setProductBrand(event.target.value)} required/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" placeholder="Description"  value={prodDes} onChange={event => setDescription(event.target.value)} required/>
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" placeholder="Price"  value={prodPrice} onChange={event => setPrice(event.target.value)} required/>
                        </Form.Group>  
                        <Button variant="primary" type="submit" controlId="submitBtn" onClick={handleCloseUpdate} >Update</Button>
                    </Form>
                </Modal.Body>
              </Modal>
{/*Delete Modal*/}
              <Modal
                show={showDelete}
                onHide={() => setShowArchive(false)}
                dialogClassName="modal-90w"
                aria-labelledby="lol"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="lol">
                    Archive Product
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form  className="mt-4" onSubmit={event=> {archiveProduct(event) } }>
                         <Form.Group className="mb-3" controlId="productName">
                            <Form.Label hidden>`${_id}`</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Product Name :{prodName}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Product Brand: {prodBrand}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description:  {prodDes}</Form.Label>   
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Price: {prodPrice}</Form.Label>
                        </Form.Group>
                         <Button variant="danger" type="submit" controlId="submitBtn" onClick={handleCloseDelete} >Archive</Button>
                    </Form>
                
                </Modal.Body>
              </Modal>
              </>

		)
}