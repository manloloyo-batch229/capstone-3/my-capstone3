import React, { useState } from "react";
import Modal1 from "./Modal";

function App() {
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div>
      <button onClick={openModal}>Open Modal</button>
      <Modal1 isOpen={isOpen} closeModal={closeModal} />
    </div>
  );
}

export default App;