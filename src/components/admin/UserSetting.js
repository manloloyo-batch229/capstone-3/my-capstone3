import UserContext from '../../UserContext.js'
//import PropagateLoader from 'react-spinners/PropagateLoader'
import {Table,Container,NavDropdown,Nav,Navbar,DropdownButton,Dropdown} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';


export default function UserSetting() {

	return(
		<div className="admin-bg p-0">
			<div className="container table-container">							
			<div className="w-100 text-white mb-3 admin-titles">
				<h3>Users List</h3>
			</div>
	
		<Table striped bordered hover variant="dark">
	      <thead>
	        <tr>
	          <th>Email</th>
	          <th>Name</th>
	          <th>UserID</th>
	          <th>isAdmin</th>
	        </tr>
	      </thead>
	      <tbody>
	        
	        	<tr >			        		
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td>
	        			<DropdownButton  id="dropdown-basic-button" className="btn-secondary"  title="True" >
                <Dropdown.Item  >True</Dropdown.Item>
                <Dropdown.Item >False</Dropdown.Item>
          	</DropdownButton>
	        		</td>
	        	</tr>	        	
	   
	      </tbody>
	    </Table>
	</div>
</div>
		)
}
