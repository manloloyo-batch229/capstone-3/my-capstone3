import React from "react";

import {Container,Row,Col, InputGroup, FormControl, ListGroup, Button, Card, Table ,Form} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2'
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App.js'

function Modal1({ isOpen, closeModal }) {

     const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);

    const data = [{
      productName: "",
      productBrand: "",
      description: "",
      price: 100,
      isActive: true
    }]
    const {productName,productBrand,description, price, _id, isActive} = data;

    const [searchInput, setSearchInput] = useState('');
    
    const [prodName, setProductName] = useState(productName);
    const [prodBrand,setProductBrand] = useState(productBrand);
    const [prodDes,setDescription] = useState(description);
    const [prodPrice,setPrice] = useState(price);
  if (!isOpen) {
    return null;
  }

  return (
    
      <Modal
                show={show}
                onHide={() => setShow(false)}
                dialogClassName="modal-90w"
                aria-labelledby="lol"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="lol">
                    Update Product
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form className="mt-4">
                         <Form.Group className="mb-3" controlId="productName">
                            <Form.Label >{_id}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control type="text" placeholder="Product name"  value={prodName} onChange={event => setProductName(event.target.value)} required/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="productBrand">
                            <Form.Label>Product Brand</Form.Label>
                            <Form.Control type="text" placeholder="Brand name"  value={prodBrand} onChange={event => setProductBrand(event.target.value)} required/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" placeholder="Description"  value={prodDes} onChange={event => setDescription(event.target.value)} required/>
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" placeholder="Price"  value={prodPrice} onChange={event => setPrice(event.target.value)} required/>
                        </Form.Group>  
                        <Button variant="danger" type="submit" controlId="submitBtn" onClick={handleClose} >Update</Button> 
                    </Form>
                </Modal.Body>
              </Modal>
   
  );
}

export default Modal1;