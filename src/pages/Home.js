import Banner from '../components/home/Banner.js'
import Highlights from '../components/home/Highlights.js'
import UserContext from '../UserContext.js'

//import PropagateLoader from 'react-spinners/PropagateLoader'
import {useState, useEffect, useContext} from 'react';
import {Form, Button,Col,Row} from 'react-bootstrap';
export default function Home() {

	const {user, setUser} = useContext(UserContext);
	console.log(user)

	return(
		
		 <div className="p-0 bannerDesign">
			<Banner/>
			
		</div>
	
		)
}