
import {Link} from 'react-router-dom'
export default function PageNotFound(){

	return(
		 <div className="text-center notFound">
        <h1 className="titleFound">404 Page Not Found :( </h1>

        <p className="backFound">Please go back to the <Link to="/">homepage</Link> :) </p>

    	</div>

		)
}