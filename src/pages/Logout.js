import{useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js'
export default  function Logout(){


const {unsetUser, setUser} = useContext(UserContext);
unsetUser();

useEffect(() => {

	setUser({token:null,id:null,isAdmin: null})
	})

	return(
			<Navigate to="/login"/>

		)
}