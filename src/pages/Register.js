import {Form, Button ,Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate,useNavigate, NavLink, Link} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Register() {
	const navigate = useNavigate();
	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const[isActive, setIsActive] = useState(false);

	useEffect(() => {
		
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[firstName,email,lastName,mobileNo,password1,password2])

	function registerUser(event) {
		// prevents page redirection via form submission
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL }/users/checkEmail`,{
			method: 'Post',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email

			})
		},[])
		.then(result => result.json())
		.then(data => {
				if(data !== true){
					fetch(`${process.env.REACT_APP_API_URL }/users/register`,{
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					},[])

					Swal.fire({
				    			title: "Registration Successfull",
				    			icon: "success",
				    			text: "Welcome to Zuitt"
		    		})
		    		navigate("/login")		
		    							
					setFirstName('')
					setLastName('')
					setEmail('');
					setMobileNo('');
					setPassword1('');
					setPassword2('');
					

					
				}else{
					Swal.fire({
				    			title: "Duplication email found",
				    			icon: "warning",
				    			text: "Please provide a different email"
		    		})
				}
		})
	
	}

	return(
			(user.token != null)?
			<Navigate to="/"/>
			:
		<Container  variant="secondary" className="col-lg-5 registerContainer text-center">
		    <Form  onSubmit={event=> registerUser(event)} className="mt-4">
		    	<h1 className="text-center">Register</h1>
		    	<Form.Group className="mb-3" controlId="firstName">
		        	<Form.Label>First Name</Form.Label>
		        	<Form.Control type="text" placeholder="Enter first name"  value={firstName} onChange={event => setFirstName(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="lastName">
		        	<Form.Label>Last Name</Form.Label>
		        	<Form.Control type="text" placeholder="Enter last name"  value={lastName} onChange={event => setLastName(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="userEmail">
		        	<Form.Label>Email address</Form.Label>
		        	<Form.Control type="email" placeholder="Enter email"  value={email} onChange={event => setEmail(event.target.value)} required/>
		        	
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="mobileNo">
		        	<Form.Label>Mobile Number</Form.Label>
		        	<Form.Control type="number" placeholder="Enter last name"  value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
		      	</Form.Group>		      	

		      	<Form.Group className="mb-3" controlId="Password1">
		        	<Form.Label>Password</Form.Label>
		        	<Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="Password2">
		        	<Form.Label>Verify Password</Form.Label>
		        	<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={event => setPassword2(event.target.value)} required />
		      	</Form.Group>

		     {/*Conditional Rendering -> If active button is clickable -> inactive button is not clickable*/}
		     
		     
		     <div className="d-inline-block ">
		     {
		     	(isActive)?	
		     	<Button variant="primary" type="submit" controlId="submitBtn" className="justify-content-center" >Register</Button>	

		     	: 
		     	<Button variant="primary" type="submit" controlId="submitBtn" className="justify-content-center" disabled>Register</Button>	
		     }

		     <Link class="btn btn-danger btn-block mx-3 loginCancelButton" to="/login">Cancel</Link>
		   	</div>
		    </Form>
		</Container>
		)
}