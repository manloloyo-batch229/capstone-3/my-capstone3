import AllProducts from '../components/admin/DashBoard.js'
import UserSetting from '../components/admin/UserSetting.js'

import {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom'
import {Nav,Button,Container} from 'react-bootstrap'
export default function Admin(){

	const [products, setProducts] = useState([]);
	useEffect(() =>{
    	fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	        .then(res => res.json())
        	.then(data => {
	            console.log(data);
	            setProducts(data.map(products => {
	            	return(
	            		<AllProducts key={products.id} productProps={products}/>
	            		)
	            }))
	        });
    }, [])
function btnhome(){

}

    return (
    	<>
    	<Container className="">
    	{/*<UserSetting/>*/}
    	</Container>
    		<h1 className="adminHeader text-center">Admin Dashboard</h1>
    		<div className="text-center">
    		     <Button  className="newProduct "variant="primary" type="button">
    			<Nav.Link  as={NavLink} exact to="/addproduct">Add Product</Nav.Link>
    		</Button>
    		
    		</div>
    		{products}
    		
    	</>

    	)

	
}