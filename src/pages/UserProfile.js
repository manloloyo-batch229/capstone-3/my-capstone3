import UserContext from '../UserContext.js'
//import PropagateLoader from 'react-spinners/PropagateLoader'
import {Table} from 'react-bootstrap';
import {useState, useContext} from 'react';
//import {Form, Button,Col,Row} from 'react-bootstrap';
export default function Profile() {

	const {user, setUser} = useContext(UserContext);
	console.log(user)
	const [products, setProducts] = useState([])
	const [loading, setLoading] = useState(true)

	const fetchProducts = async () => {
		

		const res = await fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		const data = await res.json()
	}

	return(
		<div className="admin-bg">
			<div className="container table-container">							
			<div className="w-100 text-white mb-3 admin-titles">
				<h3 className="text-center profileHeader">Users List</h3>
			</div>
				<h1 className="text-center mb-3">Ordered Products</h1>
				<Table className="tableProfile">

			      <thead>
			        <tr>
			          <th>ProductName</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th>Total Amount</th>
			        </tr>
			      </thead>
			      <tbody>
			        
			        	<tr >			        		
			        		<td>Samsung S21</td>
			        		<td>90000</td>
			        		<td>2</td>
			        		<td>180000</td>
			        	</tr>	        	
			   
			   			<tr >			        		
			        		<td>Iphone 12 Pro Max</td>
			        		<td>35000</td>
			        		<td>3</td>
			        		<td>105000</td>
			        	</tr>	
			      </tbody>
			    </Table>
					
			
			</div>
		</div>
		)
}