import {Form, Button, Container,Nav,} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate,NavLink, Link} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'


export default function Login() {
	
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password)

	
	useEffect(() => {
		
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])




function userLogin(event) {

		
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL }/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data =>{
			console.log(data)

		
			if(typeof data.access !== "undefined"){
    		localStorage.setItem('token', data.access)
    		
	    		retrieveUserDetails(data.access);
				console.log("sdiasdisa" + data.access)
		    		Swal.fire({
		    			title: "Login Successfull",
		    			icon: "success",
		    			text: "Welcome to Zuitt"
		    		})
    		}else {
    			console.log(data)
    			Swal.fire({
	    			title: "Authentication Failed",
	    			icon: "error",
	    			text: "Check your login details and try again!"
	    		}).then(function() {
				    window.location = "/login";
				});
    		}
		})


		setEmail('');
		setPassword('');
		
	}


	const retrieveUserDetails = (token) =>{
		

    	fetch(`${process.env.REACT_APP_API_URL }/users/details`, {
    		method: 'Post',
    		headers:{
    			Authorization: `access ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data =>{
    		console.log(data);
    		localStorage.setItem('id', data._id)
    		localStorage.setItem('isAdmin', data.isAdmin)
    		
    		setUser({
    			token: localStorage.getItem('token'),
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    		
    	})


	}
	console.log(user)

	return(

		(user.token != null)?
		<Navigate to="/products"/>
		
		:

		<Container  className="col-lg-6 pt-6 loginContainer">
		    <Form onSubmit={event=> userLogin(event)} >
		    	<h2 className="text-center">Login</h2>
		      <Form.Group className="mb-3" controlid="userEmail">
		        <Form.Label>Email</Form.Label>
			        <Form.Control 
			        type="email" 
			        placeholder="Enter email"  
			        value={email} 
			        onChange={event => setEmail(event.target.value)} 
			        required
			        />
	
		      </Form.Group>

		      <Form.Group className="mb-3" controlid="Password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
		      </Form.Group>

		    <div className="d-inline-block justify-content-center ">
			     {
			     	isActive?	
			     	<Button variant="success" type="submit" controlid="submitBtn" className="justify-content-center" >Login</Button>	
			     	: 
			     	<Button variant="primary" type="submit" controlid="submitBtn" className="justify-content-center" disabled>Login</Button>	
			     }	
			     	<Link class="btn btn-primary btn-block mx-3 loginCancelButton" to="/">Cancel</Link>	     
		    </div>
		    </Form>
		</Container>   
		)
}