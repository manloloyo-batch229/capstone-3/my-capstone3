import {Form, Button,Nav} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate,useNavigate,NavLink} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'



export default function AddProduct() {
	const navigate = useNavigate();

	const [productName, setProductName] = useState('');
	const [productBrand, setProductBrand] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const[isActive, setIsActive] = useState(false);

	useEffect(() => {
		
		if(productName !== '' && productBrand !== '' && description !== '' && price !== '' ){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	})


	function addProduct(event) {
		event.preventDefault();
			
		fetch(`${process.env.REACT_APP_API_URL }/products/add`,{
			method: 'POST',
			headers: {
					'Content-Type': 'application/json'
						},
			body: JSON.stringify({
				productName: productName,
				productBrand: productBrand,
				description: description,
				price: price
			})
		},[])

		Swal.fire({
	    			title: "Product Added Successfully",
	    			icon: "success",
	    			
		})
	
		navigate("/admin")						
		setProductName('')
		setProductBrand('')
		setDescription('');
		setPrice('');
		console.log()
	}
	return(

		    <Form onSubmit={event=> addProduct(event)} className="mt-4">
		    	<h1>Add Product</h1>
		    	<Form.Group className="mb-3" controlId="productName">
		        	<Form.Label>Product Name</Form.Label>
		        	<Form.Control type="text" placeholder="Product name"  value={productName} onChange={event => setProductName(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="productBrand">
		        	<Form.Label>Product Brans</Form.Label>
		        	<Form.Control type="text" placeholder="Brand name"  value={productBrand} onChange={event => setProductBrand(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="description">
		        	<Form.Label>Description</Form.Label>
		        	<Form.Control type="text" placeholder="Description"  value={description} onChange={event => setDescription(event.target.value)} required/>
		        	<Form.Text className="text-muted">
		  
		        	</Form.Text>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="price">
		        	<Form.Label>Price</Form.Label>
		        	<Form.Control type="number" placeholder="Price"  value={price} onChange={event => setPrice(event.target.value)} required/>
		      	</Form.Group>	
		      	{
		      	 
		     	(isActive)?	
		     		<Button variant="primary" type="submit" controlId="submitBtn" >Add Product</Button>
		     		: 
		     		<Button variant="danger" type="submit" controlId="submitBtn" disabled>Add Product</Button>	
		     		
		      	}
		      	<Button  className="text-center m-3" variant="primary" type="button">
    			<Nav.Link  as={NavLink} exact to="/admin">Cancel</Nav.Link>
    		</Button>
		    </Form>
		)
}